#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"
/*
exit(1) = Incorrect function usage
exit(2) = Can't open hash file
exit(3) = Can't open dictionary file
exit(4) = Couldn't crack the hash
*/

const int PASS_LEN = 20;        // Maximum any password will be
const int HASH_LEN = 33;        // Length of MD5 hash strings

// Given a target hash, crack it. Return the matching password.
char * crackHash(char * targetHash, char * dictionaryFilename)
{
    // Open the dictionary file
    FILE *w = fopen(dictionaryFilename, "r");
    if (!w)
    {
        fprintf(stderr, "Can't open %s for reading: ", dictionaryFilename);
        perror(NULL);
        exit(3);
    }

    // Loop through the dictionary file, one line at a time.
    char password[PASS_LEN];
    char *ptr;
    while (fgets(password, PASS_LEN, w) != NULL)
    {
        // Trim newline
        char *nl = strchr(password, '\n');
        if (nl) *nl = '\0';

        // Hash the password
        char *dictHash = md5(password, strlen(password));

        // Does it match the target hash?
        if (strcmp(dictHash, targetHash) == 0)
        {
            // Free up the memory, close the dictionary file, and return the address of the matching password
            ptr = password;
            free(dictHash);
            fclose(w);
            return ptr;
        }
        
        // Free up the memory
        free(dictHash);
    }

    fprintf(stderr, "Could not crack the hash.\n");
    fclose(w);
    exit(4);
    
    // Close the dictionary file
    return NULL;
}

// ./crack hashes.txt dictionary_file
int main(int argc, char *argv[])
{
    if (argc != 3) 
    {
        fprintf(stderr, "Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Open the hash file for reading.
    FILE *hashes = fopen(argv[1], "r");
    if (!hashes)
    {
        fprintf(stderr, "Can't open %s for reading: ", argv[1]);
        perror(NULL);
        exit(2);
    }

    // For each hash, crack it by passing it to crackHash
    char rawHash[HASH_LEN+1];
    char *hashPassword;
    while (fgets(rawHash, HASH_LEN+1, hashes) != NULL)
    {      
        // Trim newline
        char *nl = strchr(rawHash, '\n');
        if (nl) *nl = '\0';
        
        // Display the hash along with the cracked password:
        //   5d41402abc4b2a76b9719d911017c592 hello
        hashPassword = crackHash(rawHash, argv[2]);
        printf("%s %s\n", rawHash, hashPassword);
    }
    
    // Close the hash file
    fclose(hashes);
}
